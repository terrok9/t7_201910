package controller;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;


import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import contenedora.VOMovingViolations;
import model.data_structures.Node;
import model.data_structures.RedBlackBST;
import view.MovingViolationsManagerView;


public class Controller {
	// Componente vista (consola)
	private MovingViolationsManagerView view;
    private RedBlackBST<Integer, VOMovingViolations> arbol;
	private String[] semestre;
	/**
	 * Metodo constructor
	 */
	public Controller()
	{
		view = new MovingViolationsManagerView();
		semestre = new String[6];
		arbol = new RedBlackBST<Integer, VOMovingViolations>();
	}
	public String[] semestre(int pNumsemestre){
		if(pNumsemestre == 1){
			semestre[0] = "January";
			semestre[1] = "February";
			semestre[2] = "March";
			semestre[3] = "April";
			semestre[4] = "May";
			semestre[5] = "June";
		}
		else{
			semestre[0] = "July";
			semestre[1] = "August";
			semestre[2] = "September";
			semestre[3] = "October";
			semestre[4] = "November";
			semestre[5] = "December";
		}
		for(int i = 0; i < semestre.length; i++){
		System.out.println(semestre[i]);
		}
		return semestre;
	}
	/**
	 * Metodo encargado de ejecutar los  requerimientos segun la opcion indicada por el usuario
	 */
	public void run(){

		long startTime;
		long endTime;
		long duration;

		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		Controller controller = new Controller();

		while(!fin){
			view.printMenu();

			int option = sc.nextInt();

			switch(option){

			case 0:
				view.printMessage("Ingrese semestre a cargar (1 o 2)");
				view.printMessage("Escriba el n�mero 1");
				int semestre = sc.nextInt();
				controller.loadMovingViolation(semestre);
				break;
			case 1:
				view.printMessage("Ingrese el ObjectID");
				int objectid = sc.nextInt();
				consultarLaInformacionObjectId(objectid);
				break;
			case 2:
				view.printMessage("Ingrese rango [ObjectId menor, ObjectIdMayor] RESPECTIVAMENTE");
				view.printMessage("ObjectId menor");
				int objectmenor = sc.nextInt();
				view.printMessage("ObjectId mayor");
				int objectmayor = sc.nextInt();
				consultarEnRango(objectmenor, objectmayor);
				break;
			case 3:	
				fin = true;
				sc.close();
				break;
			}
		}
	}
	public void consultarEnRango(int menor, int mayor){
		if(menor > mayor){
			System.out.println("Pues... la idea es que ingrese primero el menor y luego el mayor :D");
		}
		else if(menor == mayor){
			consultarLaInformacionObjectId(menor);
		}
		else{
			Iterator<Integer> iter = arbol.keys(menor, mayor);
			while(iter.hasNext()){
				Node<Integer, VOMovingViolations> obj1 = arbol.buscarNodo(iter.next());
				VOMovingViolations obj11 = obj1.darValor();
				System.out.println("Location:" + obj11.getLocation() +  " AddressId:" + obj11.getAddressId() +  " StreetSegId:" + obj11.getStreetSegId() + " XCoord:" + obj11.getxCoord() + " YCoord:" + obj11.getyCoord() + " TicketIssueDate:" + obj11.getTicketIssueDate());
			}
		}
	}
	public void consultarLaInformacionObjectId(int objectid){
		Node<Integer, VOMovingViolations> obj1 = arbol.buscarNodo(objectid);
		VOMovingViolations obj11 = obj1.darValor();
		System.out.println("Location:" + obj11.getLocation() +  " AddressId:" + obj11.getAddressId() +  " StreetSegId:" + obj11.getStreetSegId() + " XCoord:" + obj11.getxCoord() + " YCoord:" + obj11.getyCoord() + " TicketIssueDate:" + obj11.getTicketIssueDate());
	}
 public void loadMovingViolation(int numsemestre){
	 semestre = semestre(numsemestre);
      int datos1 = 0;
      int datos2 = 0;
      int datos3 = 0;
      int datos4 = 0;
      int datos5 = 0;
      int datos6 = 0;
	  try{
		  JsonReader jreader1 = new JsonReader(new FileReader("data/Moving_Violations_Issued_in_"+ semestre[0] +"_2018.json"));
		  jreader1.beginArray();
		  while(jreader1.hasNext()){
			  VOMovingViolations obj1 =  creadorDeObjetos(jreader1);
			  arbol.put(obj1.objectId(), obj1);
			  datos1++;
		  }
		  jreader1.endArray();
		  System.out.println(datos1);
		  JsonReader jreader2 = new JsonReader(new FileReader("data/Moving_Violations_Issued_in_"+ semestre[1] +"_2018.json"));
		  jreader2.beginArray();
		  while(jreader2.hasNext()){
			  VOMovingViolations obj2 =  creadorDeObjetos(jreader2);
			  arbol.put(obj2.objectId(), obj2);
			  datos2++;
		  }
		  jreader2.endArray();
		  System.out.println(datos2);
		  JsonReader jreader3 = new JsonReader( new FileReader("data/Moving_Violations_Issued_in_"+ semestre[2] +"_2018.json"));
		  jreader3.beginArray();
		  while(jreader3.hasNext()){
			  VOMovingViolations obj3 =  creadorDeObjetos(jreader3);
			  arbol.put(obj3.objectId(), obj3);
			  datos3++;
		  }
		  jreader3.endArray();
		  System.out.println(datos3);
		  JsonReader jreader4 = new JsonReader( new FileReader("data/Moving_Violations_Issued_in_"+ semestre[3] +"_2018.json"));
		  jreader4.beginArray();
		  while(jreader4.hasNext()){
			  VOMovingViolations obj4 =  creadorDeObjetos(jreader4);
			  arbol.put(obj4.objectId(), obj4);
			  datos4++;
		  }
		  jreader4.endArray();
		  System.out.println(datos4);
		  JsonReader jreader5 = new JsonReader( new FileReader("data/Moving_Violations_Issued_in_"+ semestre[4] +"_2018.json"));
		  jreader5.beginArray();
		  while(jreader5.hasNext()){
			  VOMovingViolations obj5 =  creadorDeObjetos(jreader5);
			  arbol.put(obj5.objectId(), obj5);
			  datos5++;
		  }
		  jreader5.endArray();
		  System.out.println(datos5);
		  JsonReader jreader6 = new JsonReader( new FileReader("data/Moving_Violations_Issued_in_"+ semestre[5] +"_2018.json"));
		  jreader6.beginArray();
		  while(jreader6.hasNext()){
			  VOMovingViolations obj6 =  creadorDeObjetos(jreader6);
			  arbol.put(obj6.objectId(), obj6);
			  datos6++;
		  }
		  jreader6.endArray();
		  jreader1.close();
		  jreader2.close();
		  jreader3.close();
		  jreader4.close();
		  jreader5.close();
		  jreader6.close();
		  System.out.println(datos6);
		  System.out.println("Tama�o del �rbol" + arbol.size());
		  System.out.println("Altura del �rbol" + arbol.height());
		  System.out.println("Check del �rbol bien cargado" + arbol.check());
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
 }
 public VOMovingViolations creadorDeObjetos(JsonReader reader) throws IOException{
		String Location="";
		String TotalPaid="";
		String AccidentIndicator="";
		LocalDateTime TIDate=null;
		String ViolationC="";
		String FAMT="";
		Integer Street_Id=0;
		Integer Address_Id=0;
		String X="";
		String Y="";
		String P1="";
		String ObjectId="";
		String P2="";
		String ViolationDescription="";
		//Crear el objeto con la informaci�n de una multa...
		reader.beginObject();
		while (reader.hasNext()){
			//Me aseguro de que coja un objeto v�lido
			if(reader.peek() != JsonToken.NULL){
				String linea = reader.nextName();
				if (linea.equals("OBJECTID")) 
				{
					ObjectId= reader.nextString();	
				} 
				else if (linea.equals("LOCATION")) 
				{
					Location = reader.nextString();
				} 
				else if (linea.equals("XCOORD")) 
				{
					X= reader.nextString();
				} 
				else if (linea.equals("YCOORD")) 
				{
					Y= reader.nextString();
				} 
				else if (linea.equals("ADDRESS_ID")) 
				{
					try 
					{
						Address_Id = reader.nextInt();
					} 
					catch (Exception e) 
					{
						//Si no es un int lo transforma en -10 para limpiar la base
						Address_Id=-10;
					}
					
				}  
				else if (linea.equals("STREETSEGID")) 
				{
					try 
					{
						Street_Id = reader.nextInt();
					} 
					catch (Exception e) 
					{
						//Si no es un int lo transforma en -10 para limpiar la base
						Street_Id=-10;
					}
				} 
				else if (linea.equals("VIOLATIONDESC")) 
				{
					ViolationDescription = reader.nextString();
				} 
				else if (linea.equals("TOTALPAID")) 
				{
					TotalPaid = reader.nextString();
				} 
				else if (linea.equals("ACCIDENTINDICATOR")) 
				{
					AccidentIndicator = reader.nextString();
				}
				else if (linea.equals("TICKETISSUEDATE")) 
				{
					TIDate = convertirFecha_Hora_LDT(reader.nextString());
				}
				else if (linea.equals("VIOLATIONCODE")) 
				{
					ViolationC = reader.nextString();
				}
				else if (linea.equals("FINEAMT")) 
				{
					FAMT = reader.nextString();
				}
				else if (linea.equals("PENALTY1")) 
				{
					P1 = reader.nextString();
				}
				else if (linea.equals("PENALTY2")) 
				{
					try 
					{
						P2 = reader.nextString();
					} 
					catch (Exception e) 
					{
						P2 = "P2";
						//DO NOTHING
					}
				}
				//Si el objeto no cumple los requisitos para ser JsonObject
				else 
				{
					reader.skipValue();
				}
			}
			//Salta al siguiente si est� vacio
			else
			{
				reader.skipValue();
			}
		}
		reader.endObject();
		//Llenamos el objeto con la info dada
		VOMovingViolations object =  new VOMovingViolations(Integer.parseInt(ObjectId),Location, Address_Id, Street_Id, Double.parseDouble(X), Double.parseDouble(Y),Double.parseDouble(FAMT),Double.parseDouble(TotalPaid), P1,P2,AccidentIndicator,TIDate,ViolationC,ViolationDescription);
		return object;
			}
 /**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}

	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	 private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)

  {

                 return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));

  }
 }

