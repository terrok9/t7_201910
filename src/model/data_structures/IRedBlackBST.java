package model.data_structures;
import java.util.Iterator;

public interface IRedBlackBST<K extends Comparable<K>, V> {

	public int size();

	public boolean isEmpty();

	public V get(K key);

	public int getHeight(K key);

	public int height();

	boolean contains(K key);

	void put(K key, V value);

	public K min();

	public K max();

	public Iterator<K> keys();

	public Iterator<V> valuesInRange(K init, K end);

	public Iterator<K> keysInRange(K init, K end);
}