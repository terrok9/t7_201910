package contenedora;

import java.time.LocalDateTime;

public class VOMovingViolations {

	private Integer ObjectId;

	private String Location;

	private Integer AddressId;

	private Integer StreetSegId;

	private double xCoord;

	private double yCoord;



	private double FineAmount;

	private double valor;

	private String penalty1;

	private String penalty2;

	private String accidente;

	private LocalDateTime Date;

	private String violationCode;

	private String violation;

	public VOMovingViolations(Integer pObjectId, String pLocation, Integer pAdressId, Integer pStreetSegId, double pXcoord, double pYcoord, double pFineAmount, double pValor, String pPenalty1, String pPenalty2, String pAccidente, LocalDateTime pDate, String pViolationCode, String pViolation){
		ObjectId = pObjectId;
		Location = pLocation;
		AddressId = pAdressId;
		StreetSegId = pStreetSegId;
		xCoord = pXcoord;
		yCoord = pYcoord;
		
		FineAmount = pFineAmount;
		valor = pValor;
		penalty1 = pPenalty1;
		penalty2 = pPenalty2;
		accidente = pAccidente;
		Date = pDate;
		violationCode = pViolationCode;
		violation = pViolation;
	}

	/**
	 * @return id - Identificador �nico de la infracci�n
	 */
	public Integer objectId() {
		// TODO Auto-generated method stub
		return ObjectId;
	} 


	/**
	 * @return location - Direcci�n en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return Location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracci�n .
	 */
	public LocalDateTime getTicketIssueDate() {
		// TODO Auto-generated method stub
		return Date;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pag� el que recibi� la infracci�n en USD.
	 */
	public double getTotalPaid() {
		// TODO Auto-generated method stub
		return valor;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidente;
	}

	/**
	 * @return description - Descripci�n textual de la infracci�n.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violation;
	}

	public Integer getAddressId() {
		// TODO Auto-generated method stub
		return AddressId;
	}

	public Integer getStreetSegId() {
		// TODO Auto-generated method stub
		return StreetSegId;
	}
	public String getPenalty1(){
		return penalty1;
	}
	public String getPenalty2(){
		return penalty2;
	}
	public String getViolationCode(){
		return violationCode;
	}
	public double getFineAmount(){
		return FineAmount;
	}

	public double getxCoord() {
		return xCoord;
	}

	public double getyCoord() {
		return yCoord;
	}

	

}

