package view;



public class MovingViolationsManagerView {

	/**
	 * Constante con el numero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;
	
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 2----------------------");
		System.out.println("0. Cargar datos del semestre");
		System.out.println("1. Consultar la información asociada a un valor ObjectId. El usuario ingresa el ObjectId.");
		System.out.println("2. Consultar los ObjectIds de las infracciones que se encuentren registrados en un rango dado por [ObjectId menor, ObjectIdMayor]. ");
		System.out.println("3. Salir");
		
		
		
	}
	
	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}
	
	/**
	 * Método de ejemplo del proyecto 2
	public void printResumenLoadMovingViolations(EstadisticasCargaInfracciones resultados) {
		int mes = 1;
		System.out.println("Total de Infracciones :" + resultados.darTotalInfracciones());
		for (int infraccionesXMes : resultados.darNumeroDeInfraccionesXMes())
		{
			System.out.println("Infracciones mes:" + mes + " = " + infraccionesXMes);
			mes++;
		}
		double [] minimax = resultados.darMinimax();
		System.out.println("Minimax: [" + minimax[0] + ", " + minimax[1] + "], [" + minimax[2] + ", " + minimax[3] + "]");
	}
	**/
	
	
	
	
}
