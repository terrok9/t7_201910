package model.data_structures;

import java.util.Iterator;



import static org.junit.jupiter.api.Assertions.*;



public class RedBlackBSTTest{
	class Prueba {
		int id;
		String cadena;

		Prueba(int id, String cadena) {
			this.id = id;
			this.cadena= cadena;
		}

		public boolean equals(Object obj) {
			Prueba prueba = (Prueba) obj;
			return prueba.id == this.id && prueba.cadena.equals(this.cadena);
		}


	}

	RedBlackBST<Integer, Prueba> arbol;

	void scenario1() {
		arbol = new RedBlackBST<>();
	}

	void scenario2() {
		arbol = new RedBlackBST<>();

		for(int i = 1; i <= 7; i++) {
			Prueba prueba = new Prueba(i, "cadena" + i);
			arbol.put(prueba.id, prueba);
		}
	}

	void testPut() {
		//Caso 1
		scenario1();
		Prueba prueba = new Prueba(1, "String1");
		arbol.put(prueba.id, prueba);
		assertFalse(arbol.buscarNodo(prueba.id).esEnlaceRojo());
		assertEquals(1, arbol.size(), "El peso no corresponde.");
		assertEquals(0, arbol.height(), "La altura no corresponde.");

		scenario2();
		for(int i = 1; i <= 7; i++) {
			Node<Integer, Prueba> nodo = arbol.buscarNodo(i);

			if(nodo.esEnlaceRojo()) {
				fail("No deber�an haber enlaces rojos.");
			}
		}
		assertEquals(7, arbol.size(), "El peso no corresponde.");
		assertEquals(2, arbol.height(), "La altura no corresponde.");

		scenario1();
		Prueba prueba1 = new Prueba(1, "cadena1");
		Prueba prueba2 = new Prueba(2, "cadena2");
		arbol.put(prueba1.id, prueba1);
		arbol.put(prueba2.id, prueba2);

		assertEquals(arbol.darRaiz().darValor(), prueba2, "La ra�z deber�a tener la llave 2");
		assertTrue(arbol.buscarNodo(1).esEnlaceRojo(), "El enlace deber�a ser rojo.");

		scenario1();
		arbol.put(3, new Prueba(3, "cadena3"));
		arbol.put(2, new Prueba(2, "cadena2"));
		assertTrue(arbol.buscarNodo(2).esEnlaceRojo(), "El enlace deber�a ser rojo");

		arbol.put(4, new Prueba(4, "cadena4"));
		assertFalse(arbol.buscarNodo(2).esEnlaceRojo(), "El enlace deber�a ser negro");
		assertFalse(arbol.buscarNodo(4).esEnlaceRojo(), "El enlace deber�a ser negro");
	}

	void testGet() {

		scenario1();
		assertNull(arbol.get(1), "El objeto no deber�a existir.");

		scenario2();
		try {
			assertEquals(2, arbol.get(2).id, "El objeto retornado no corresponde.");
		} catch (NullPointerException e) {
			fail("El objeto deber�a existir.");
		}
	}

	void testMin() {
		scenario2();
		assertEquals(Integer.valueOf(1), arbol.min(), "El m�nimo no corresponde.");
	}
	void testMax() {
		scenario2();
		assertEquals(Integer.valueOf(7), arbol.max(), "El m�ximo no corresponde.");
	}

	void testSize() {

		scenario1();
		assertEquals(0, arbol.size(), "El peso no corresponde.");

		scenario2();
		assertEquals(7, arbol.size(), "El peso no corresponde.");
	}

	void testHeight() {

		scenario1();
		assertEquals(0, arbol.height(), "La altura no corresponde.");

		scenario2();
		assertEquals(2, arbol.height(), "La altura no corresponde.");

		arbol.put(8, new Prueba(8, "cadena8"));
		assertEquals(2, arbol.height(), "La altura no corresponde.");
	}

	void testIsEmpty() {

		scenario1();
		assertTrue(arbol.isEmpty(), "El �rbol deber�a estar vac�o.");

		scenario2();
		assertFalse(arbol.isEmpty(), "El �rbol deber�a tener elementos.");
	}

	void testContains() {

		scenario1();
		assertFalse(arbol.contains(0), "El objeto no deber�a existir.");

		scenario2();
		assertTrue(arbol.contains(1), "El objeto deber�a existir.");
		assertFalse(arbol.contains(9), "El objeto no deber�a existir.");
	}

	void testCheck() {
		//Caso 1
		scenario2();
		assertTrue(arbol.check(), "El arbol contiene errores.");
	
		arbol.put(0, new Prueba(0, "cadena0"));
		arbol.put(8, new Prueba(8, "cadena8"));
		arbol.put(9, new Prueba(9, "cadena9"));
		arbol.put(10, new Prueba(10, "cadena10"));
		assertTrue(arbol.check(), "El arbol contiene errores.");
		
		scenario2();
		arbol.put(9, new Prueba(9, "cadena8"));
		arbol.put(8, new Prueba(8, "cadena8"));
		assertTrue(arbol.check(), "El arbol contiene errores.");
		
	}

	void testKeys() {
		scenario2();

		Iterator<Integer> iter = arbol.keys();
		int i = 1;
		while(iter.hasNext()) {
			int key = iter.next();

			if(key != i) {
				fail("Las llaves no se est�n recorriendo en el orden correcto.");
			}

			System.out.println(key);
			
			i++;
		}
	}

	void testValuesInRange() {
		scenario2();

		Iterator<Prueba> iter = arbol.valuesInRange(2,6);
		int i = 2;
		while(iter.hasNext()) {
			Prueba prueba = iter.next();

			if(prueba.id != i) {
				fail("Las llaves no se est�n recorriendo en el orden correcto.");
			}

			if(prueba.id < 2 || prueba.id > 6) {
				fail("No se est� recorriendo dentro del rango establecido.");
			}

			Prueba esperado = new Prueba(i, "cadena" + i); 
			assertEquals(esperado, prueba, "El objeto retornado no es el esperado.");

			i++;
		}
	}

	void testKeysInRange() {
		scenario2();

		Iterator<Integer> iter = arbol.keysInRange(2,6);
		int i = 2;
		while(iter.hasNext()) {
			int prueba = iter.next();

			if(prueba != i) {
				fail("Las llaves no se est�n recorriendo en el orden correcto.");
			}

			if(prueba < 2 || prueba > 6) {
				fail("No se est� recorriendo dentro del rango establecido.");
			}
			
			assertEquals(i, prueba, "El objeto retornado no es el esperado.");

			i++;
		}
	}
}
